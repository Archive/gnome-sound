#include <gnome-sound.h>

int main()
{
	int sample;

	gnome_sound_init(0);
	gnome_sound_play("/tmp/test.wav");
	sleep(5);
	sample = gnome_sound_sample_load("testsample", "/tmp/test.wav");
	gnome_sound_play_sample_id(sample);
	sleep(5);
	gnome_sound_play_sample("testsample");
	gnome_sound_sample_unload("testsample");

	gnome_sound_shutdown();
	
	return 0;
}
